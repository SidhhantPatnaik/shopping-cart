package com.java.shoppingcart.constants;

public interface AppConstants
{
	
	public static String FORWARD_SLASH = "/";
	public static String ADD_ITEM = "/addItem";
	public static String ADD_ITEMS = "/addItems";
	public static String GET_ITEM_BY_ID = "/getItemById";
	public static String UPDATE_ITEM = "/updateItem";
	public static String DELETE_ITEM_BY_ID = "/deleteItemById";
	public static String PREPARE_AND_SAVE_CART_ITEM = "/prepareAndSaveCartItem";
	public static String FIND_BY_NAME_AND_EXPIRY_DATE = "/findByNameAndExpiryDate";
	
	
	public static String FIND_BY_PAYMENT_MODE = "/findByPaymentMode";
	public static String FIND_BY_CREATED_DATE = "/findByCreatedDate";

}
