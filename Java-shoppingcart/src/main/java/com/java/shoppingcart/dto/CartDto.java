package com.java.shoppingcart.dto;

import java.io.Serializable;

public class CartDto implements Serializable
{
	private Long itemId;
	
	private Integer quantity;
	
	private String paymentMode;
	
	public CartDto() {
		// TODO Auto-generated constructor stub
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	@Override
	public String toString() {
		return "CartDto [itemId=" + itemId + ", quantity=" + quantity + ", paymentMode=" + paymentMode + "]";
	}
	
	
}
