package com.java.shoppingcart.dto;

import java.io.Serializable;

public class ItemDto implements Serializable
{
	
	private Long id;
	
	private Integer price;
	
	private Integer quantity;
	
	public ItemDto() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ItemDto [id=" + id + ", price=" + price + ", quantity=" + quantity + "]";
	}
	
	
	
	}
