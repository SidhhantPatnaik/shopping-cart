package com.java.shoppingcart.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.java.shoppingcart.constants.SqlConstants;

@Entity
@Table(name = SqlConstants.CART_INFO)
public class Cart implements Serializable
{
	@Id
	@GenericGenerator(name = "c_auto", strategy = "increment")
	@GeneratedValue(generator = "c_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "item_id")
	private Long itemId;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "payment_Mode")
	private String paymentMode;
	
	@Column(name = "total_amount")
	private Integer totalAmount;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	public Cart() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", itemId=" + itemId + ", quantity=" + quantity + ", paymentMode=" + paymentMode
				+ ", totalAmount=" + totalAmount + ", createdDate=" + createdDate + "]";
	}
	
	

}
