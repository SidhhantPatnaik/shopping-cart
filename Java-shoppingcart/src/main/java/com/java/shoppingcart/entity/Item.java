package com.java.shoppingcart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.java.shoppingcart.constants.SqlConstants;

@Entity
@Table(name = SqlConstants.ITEM_INFO)
public class Item  implements Serializable
{
	
	@Id
	@GenericGenerator(name = "i_auto",strategy = "increment")
	@GeneratedValue(generator = "i_auto")
	@Column(name = SqlConstants.ITEM_ID)
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "price")
	private Integer price;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "specification")
	private String specification;
	
	@Column(name = "manufacture_date")
	private String manufactureDate;
	
	@Column(name = "expiry_date")
	private String expiryDate;
	
	
	public Item() {
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getPrice() {
		return price;
	}


	public void setPrice(Integer price) {
		this.price = price;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public String getSpecification() {
		return specification;
	}


	public void setSpecification(String specification) {
		this.specification = specification;
	}


	public String getManufactureDate() {
		return manufactureDate;
	}


	public void setManufactureDate(String manufactureDate) {
		this.manufactureDate = manufactureDate;
	}


	public String getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", price=" + price + ", quantity=" + quantity + ", specification="
				+ specification + ", manufactureDate=" + manufactureDate + ", expiryDate=" + expiryDate + "]";
	}
	
	
	
}
