package com.java.shoppingcart.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.shoppingcart.dto.CartDto;
import com.java.shoppingcart.dto.ItemDto;
import com.java.shoppingcart.entity.Cart;
import com.java.shoppingcart.entity.Item;
import com.java.shoppingcart.repository.CartRepository;
import com.java.shoppingcart.repository.ItemRepository;

import ch.qos.logback.core.pattern.FormatInfo;

@Service
public class AppService
{
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private CartRepository cartRepository;
	
	
	
	public AppService() {
		// TODO Auto-generated constructor stub
	}
	
	public void addItem(Item item)
	{
		itemRepository.save(item);
	}
	
	public void addItems(List<Item> items)
	{
		itemRepository.saveAll(items);
	}
	
	public Item getItemById(Long id)
	{
		 Optional<Item> item = itemRepository.findById(id);
		 return item.get();
	}
	
	public void updateItem(ItemDto itemDto)
	{
		Item item = getItemById(itemDto.getId());
		item.setPrice(itemDto.getPrice());
		item.setQuantity(itemDto.getQuantity());
		System.out.println(item);
		
		itemRepository.save(item);
	}
	
	public void deleteItemById(Long id)
	{
		itemRepository.deleteById(id);
	}
	
	public Cart sellItem(CartDto cartDto)
	{
		Cart cart = new Cart();
		Item item  = getItemById(cartDto.getItemId());
		
		cart.setItemId(cartDto.getItemId());
		cart.setQuantity(cartDto.getQuantity());
		cart.setPaymentMode(cartDto.getPaymentMode());
		cart.setTotalAmount(cartDto.getQuantity() * item.getPrice());
		cart.setCreatedDate(new Date());
		item.setQuantity(item.getQuantity()-cartDto.getQuantity());
		 
		itemRepository.save(item);
		
		return cart;
				
	}
	
	public void prepareAndSaveCartItem(CartDto cartDto)
	{
		Cart cart = sellItem(cartDto);
		cartRepository.save(cart);
		
	}
	
	public List<Item> findByNameAndExpiryDate(String name,String expiryDate)
	{
		return itemRepository.findByNameAndExpiryDate(name, expiryDate);
	}
	
	public List<Cart> findByPaymentMode(String paymentMode)
	{
		return cartRepository.findByPaymentMode(paymentMode);
	}
	
	public List<Cart> findByCreatedDate(Date createdDate)
	{		
		return cartRepository.findByCreatedDate(createdDate);
	}

}
