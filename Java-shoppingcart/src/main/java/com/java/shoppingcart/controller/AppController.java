package com.java.shoppingcart.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.shoppingcart.constants.AppConstants;
import com.java.shoppingcart.constants.SqlConstants;
import com.java.shoppingcart.dto.CartDto;
import com.java.shoppingcart.dto.ItemDto;
import com.java.shoppingcart.entity.Cart;
import com.java.shoppingcart.entity.Item;
import com.java.shoppingcart.repository.ItemRepository;
import com.java.shoppingcart.service.AppService;

@RestController
@RequestMapping(value = AppConstants.FORWARD_SLASH)
public class AppController 
{
	@Autowired
	private AppService appService;
	
	public AppController() {
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping(value = AppConstants.ADD_ITEM)
	public void addItem(@RequestBody Item item)
	{
		appService.addItem(item);
	}
	
	@PostMapping(value = AppConstants.ADD_ITEMS)
	public void addItems(@RequestBody List<Item> items)
	{
		appService.addItems(items);
	}
	
	@GetMapping(value = AppConstants.GET_ITEM_BY_ID)
	public @ResponseBody Item getItemById(@RequestHeader(value = SqlConstants.ITEM_ID) Long id)
	{
		return appService.getItemById(id);
	}
	
	@PutMapping(value = AppConstants.UPDATE_ITEM)
	public void updateItem(@RequestBody ItemDto itemDto)
	{
		appService.updateItem(itemDto);
	}
	
	@DeleteMapping(value = AppConstants.DELETE_ITEM_BY_ID)
	public void deleteItemById(@RequestHeader(value = SqlConstants.ITEM_ID) Long id)
	{
		appService.deleteItemById(id);
	}
	
	@PostMapping(value = AppConstants.PREPARE_AND_SAVE_CART_ITEM)
	public void prepareAndSaveCartItem(@RequestBody CartDto cartDto)
	{
		appService.prepareAndSaveCartItem(cartDto);
	}
	
	@GetMapping(value = AppConstants.FIND_BY_NAME_AND_EXPIRY_DATE)
	public @ResponseBody List<Item> findByNameAndExpiryDate(@RequestHeader("name")String name,@RequestHeader("expiryDate")String expiryDate)
	{
		return appService.findByNameAndExpiryDate(name, expiryDate);
	}
	
	@GetMapping(value = AppConstants.FIND_BY_PAYMENT_MODE)
	public @ResponseBody List<Cart> findByPaymentMode(@RequestHeader("paymentMode") String paymentMode)
	{
		return appService.findByPaymentMode(paymentMode);
	}
	
	@GetMapping(value = AppConstants.FIND_BY_CREATED_DATE)
	public @ResponseBody List<Cart> findByCreatedDate(@RequestHeader("createdDate") Date createdDate)
	{
		return appService.findByCreatedDate(createdDate);
	}

}
