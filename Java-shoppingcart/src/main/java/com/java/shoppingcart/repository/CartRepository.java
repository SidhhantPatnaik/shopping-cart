package com.java.shoppingcart.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.java.shoppingcart.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>
{

	@Query("from Cart where paymentMode=:paymentMode")
	public List<Cart> findByPaymentMode(String paymentMode);
	
	
	@Query("from Cart where createdDate=:createdDate")
	public List<Cart> findByCreatedDate(Date createdDate);
	
}
