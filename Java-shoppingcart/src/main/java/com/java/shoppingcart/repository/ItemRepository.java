package com.java.shoppingcart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.java.shoppingcart.entity.Item;


//internally a class is created of our ItemRepository interface .
//and the object of that class is getting created hence @Repository by @EnableAutoConfiguration+

@Repository //can be skipped
public interface ItemRepository extends JpaRepository<Item, Long> 
{
	@Query("from Item where name=:name and expiryDate=:expiryDate") //can be skipped
	public List<Item> findByNameAndExpiryDate(String name,String expiryDate);
}
